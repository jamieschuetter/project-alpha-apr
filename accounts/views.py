from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login


# Create your views here.
def signup(request):
    if request.method == "GET":
        form = UserCreationForm()
        return render(request, "registration/signup.html", {"form": form})
    else:
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password1"]
            password = request.POST["password2"]
            user = User.objects.create_user(
                username=username, password=password
            )
            login(request, user)
            return redirect("home")
        else:
            return render(request, "registration/signup.html", {"form": form})
